#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# The versions of python currently supported
PYVERS = $(shell pyversions -vr debian/control)

configure: configure-stamp
configure-stamp:
	dh_testdir
	touch $@

build-common: configure build-common-stamp
build-common-stamp:
	dh_testdir
	touch $@

build: build-common build-stamp
build-stamp: $(PYVERS:%=build-python%)
	touch $@
build-python%:
	python$* setup.py build

clean:
	dh_testdir
	dh_testroot
	rm -f *-stamp
	rm -rf compile build
	find -name '*.py[co]' -exec rm -f {} \;
	dh_clean
	rm -f functional_docs.html

install: build install-prereq $(PYVERS:%=install-python%)

install-prereq:
	dh_testdir
	dh_testroot
	dh_clean -k

install-python%:
	python$* setup.py install --prefix=/usr --root `pwd`/debian/python-goopy
	find debian/python-goopy -name '*.py[co]' -exec rm -f {} \;
	sed -i -e '1 s|.*|#!/usr/bin/python|' ./debian/python-goopy/usr/lib/python$*/*-packages/goopy/functional_unittest.py
	pydoc$* -w ./goopy/functional.py
	mv functional.html functional_docs.html
	dh_installdocs functional_docs.html

binary-arch: build install

# Build architecture-independent files here.
# Pass -i to all debhelper commands in this target to reduce clutter.
binary-indep: build install
	dh_testdir -i
	dh_testroot -i
	dh_installdocs -i
	dh_installexamples -i
	dh_installchangelogs -i
	dh_pysupport -i
	dh_link -i
	dh_compress -i -X.py
	dh_fixperms -i -X.py
	find -name 'functional_unittest.py' | xargs chmod 755
	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary: binary-indep
.PHONY: build clean binary-indep binary-arch binary install configure
